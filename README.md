# About

The file settings.json is a user settings file for [Visual Studio Code](https://code.visualstudio.com/) that was modified with the purpose of achieving Syntax Highlighting that resembles more [Jetbrains Pycharm IDE](https://www.jetbrains.com/pycharm/). The base theme used was the [Darcula Extended](https://marketplace.visualstudio.com/items?itemName=smlombardi.darcula-extended) theme.

Feel free to use and modify to your taste.
See [VSCode documentation](https://code.visualstudio.com/blogs/2017/02/08/syntax-highlighting-optimizations) on syntax highlighting optimization for further information.

### Code Snippets

These are some examples to illustrate the syntax modifications:

**Python example:**

![Python](/examples/python_example.png)

**Kivy example:**

![Kivy](/examples/Kivy_example.png)

**Full Editor:**

![VSC](/examples/editor_looks.png)
