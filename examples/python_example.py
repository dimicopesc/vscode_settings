from pprint import pprint

class ChildClass(ParentClass):
    """This is an example class to show the syntax colors modified by
    user settings shared on this repository"""

    def __init__(self, *args):
        super(ChildClass, self).__init__(*args))
        self.foo = 'bar'
        self.data_dict = {}

    def some_method(self, *args):
        if self.foo == 'bar':
            #Do something

        pprint("This method is doing something")
        print("This method is doing more things")

    def file_reading(self):
        with open('example_file', 'r') as file:
            self.data_dict = json.load(file)
    
    @staticmethod
    def iterating():
        for i in range(1,10):
            print("i is: {}".format(i))

    @staticmethod
    def more_specific(arg1=True, arg2=None):
        pass


c = ChildClass()
c.more_specific(arg1=False, arg2=3)


